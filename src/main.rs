use dotenv::dotenv;
use std::{
    env,
    fmt::Display,
    fs::File,
    io::{BufReader, Read, Write, BufRead},
    path::Path,
};

use color_eyre::{
    eyre::{Context, Result},
    Section,
};
use llm::{Model, ModelParameters, InferenceSessionConfig};

fn main() -> Result<()> {
    color_eyre::install()?;

    let input: String = BufReader::new(File::open("diff.txt").unwrap())
        .lines()
        .flatten()
        .map(|line| "diff: ".to_owned() + line.as_str() + "\n")
        .collect();

    let input_context_1 =
        format!("# Diff of change in the code\n{input}\n# Informative Git Commit Message for the code\ngit commit -m \"");
    let input_context_1: &str = input_context_1.as_ref();

    let output = load_and_run_model(input_context_1)?;

    println!("IT WORKED: {output}");
    Ok(())
}

fn load_and_run_model(prompt: &str) -> Result<String> {
    println!("Loading model");
    dotenv().ok();

    let model_path =
        env::var("MODEL_PATH").suggestion("Set the MODEL_PATH environment variable")?;
    let model_path = Path::new(model_path.as_str());

    let llama = llm::load::<llm::models::Llama>(
        model_path,
        llm::TokenizerSource::Embedded,
        ModelParameters {
            use_gpu: true,
            gpu_layers: Some(8),
            ..Default::default()
        },
        llm::load_progress_callback_stdout,
    )
    .suggestion("Make sure the model is downloaded or in the correct folder")?;

    let request = llm::InferenceRequest {
        prompt: prompt.into(),
        play_back_previous_tokens: false,
        maximum_token_count: None,
        parameters: &llm::InferenceParameters::default(),
    };

    let mut session = llama.start_session(InferenceSessionConfig {
        n_batch: 512,
        ..Default::default()
    });

    let mut message: String = "".to_owned();

    let stats = session.infer::<std::convert::Infallible>(
        &llama,
        &mut rand::thread_rng(),
        &request,
        &mut Default::default(),
        |r| match r {
            llm::InferenceResponse::InferredToken(t) if t.contains("\"") => Ok(llm::InferenceFeedback::Halt),
            llm::InferenceResponse::InferredToken(t) => {
                print!("[{t}]");
                std::io::stdout().flush().unwrap();

                message += t.as_str();

                Ok(llm::InferenceFeedback::Continue)
            }
            _ => Ok(llm::InferenceFeedback::Continue),
        },
    )?;

    println!("{stats}");
    println!("{message}");

    Ok(message)
}

#[derive(thiserror::Error, Debug)]
enum OhNo {
    #[error("Oh No...")]
    None,
    #[error("{0} Oh No...")]
    Reason(String),
}

// impl Display for OhNo {
//     fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
//         write!(f, "Oh No...")
//     }
// }
